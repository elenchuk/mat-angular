import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, NgForm, Validators} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  profileForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
    pass: new FormControl('', Validators.required),
  });

  constructor() {
  }

  ngOnInit() {
  }

  onSubmit() {
    console.log(this.profileForm)
  }

  get email() {
    return this.profileForm.get('email');
  }

  get pass() {
    return this.profileForm.get('pass');
  }

}
